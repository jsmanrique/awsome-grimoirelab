# AWSome-GrimoireLab

Customized [GrimoireLab](https://chaoss.github.io/grimoirelab) to be deployed in 
[AWS Lightsail](https://aws.amazon.com/lightsail/) and using [Open Distro for 
Elasticsearch](https://opendistro.github.io/for-elasticsearch/)  instead of 
Elastic stack.

# Requirements

* [Ansible](https://www.ansible.com/) (for cloud environment set up)
* [Terraform](https://www.terraform.io/) (for remote software management)
* Your SSH public key added [to your AWS Lightsail account](https://lightsail.aws.amazon.com/ls/docs/en_us/articles/understanding-ssh-in-amazon-lightsail)
This is a bit *tricky* because to do so, you should start to create an instance 
in the web console, and select to upload your SSH public key file. The default 
configuration expects that AWS names it `id_rsa`. If you have given it another 
name, you must change `variable "key_par_name"` in the [terraform.tf](terraform.tf) file.
* [Your AWS access key and secret key](https://lightsail.aws.amazon.com/ls/docs/en_us/articles/lightsail-how-to-set-up-access-keys-to-use-sdk-api-cli).

# How to run it

# Step 1: Clone, or fork and clone, this repository

Clone it into your laptop / machine. I've only tested it in Linux, so I cannot 
promise it'll work the same in MacOS or MS Windows. It will create a folder 
called `awesome-grimoirelab`.
```
git clone https://gitlab.com/jsmanrique/awsome-grimoirelab
```

Inside such folder create a file called `terraform.tfvars` with the following 
format and content:
```
AWSAccessKeyId = "<YOUR_AWS_ACCESS_KEY_ID>"
AWSSecretKey = "<YOUR_AWS_SECRET_KEY>"
```

# Step 3. Initialize Terraform:
```
terraform init
```

# Step 4. Create your AWS Ligthsail instance

It will read both your `terraform.tfvars` and `provision.tf` files:
```
terraform apply -var name=<GIVE_IT_A_NAME>
```

During the process, Terraform might ask for confirmation to apply the changes you
have requested. If you want to say `yes` to everything, just run:
```
terraform apply -auto-approve -var name=<GIVE_IT_A_NAME>
```

At the end of the process, Terraform outputs the public IP to access to the
machine you have created. Something like:
```
Apply complete! Resources: 2 added, 0 changed, 0 destroyed.

Outputs:

public_ip = 157.245.85.159
```

**Note**: It seems you cannot open `5601` port using Terraform 
([terraform-providers/terraform-provider-aws/issues/700](https://github.com/terraform-providers/terraform-provider-aws/issues/700)),
so you'll need to open it using [your AWS Lightsail instances](https://lightsail.aws.amazon.com/ls/webapp/home)
web interface:

![Lightsail Firewall Management screenshot](lightsail-firewall-management-screenshot.jpg)

# Step 5. Write your custom GrimoireLab settings

You need to specify which projects you want to track and some configuration
variables.

Create a folder to store the settings files. Use 
[grimoirelab-tmpl-settings](grimoirelab-tmpl-settings) as reference.

**IMPORTANT**: Don't forget to fill `api_token` fields with your token, or API Key, in
`setup-odfe.cfg` if you plan to gather data from GitHub, GitLab, Meetup, or
StackOverflow.

# Step 6. Deploy GrimoireLab

Run:
```
ansible-playbook -i <PUBLIC_IP>, deploy.yml -e "src_settings_path=<PATH_TO_SETTINGS>"
```

After a while, dashboard should public at `https://<PUBLIC_IP>:5061`, and 
[HatStall](https://github.com/chaoss/grimoirelab-hatstall) in `https://<PUBLIC_IP>:8000`.
In both cases, user:password is `admin:admin`.